function generarBoleto() {

    const precio = parseFloat(document.getElementById("precio").value);
    const tipoviaje = parseInt(document.getElementById("tipoviaje").value);
    
    let subtotal = 0;
    
    if (tipoviaje === 1) {
    subtotal = precio;
    } else if (tipoviaje === 2) {
    subtotal = precio + (precio * 0.8);
    }

    const impuesto = subtotal * 0.16;
    const pagototal = subtotal + impuesto;

    document.getElementById("subtotal").value = subtotal.toFixed(2);
    document.getElementById("impuesto").value = impuesto.toFixed(2);
    document.getElementById("pagototal").value = pagototal.toFixed(2);
    }